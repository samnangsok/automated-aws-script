 #!/usr/bin/env bash

useradd -m -d /home/devops.interview -s /bin/bash devops.interview
mkdir -p /home/devops.interview/.ssh/
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEA+jwsT829dvwk72kWlwBx4p7+PXlZbixdD/qAx5xNE3SSt3Kdw3MLYnz+pUL8FJSg8G7dkQ9FZEEbPylDXTfSJ0njUxJdLebiNzWJPA9W3aDBpIoZcI6YuNF95vyixZO0nWgvMyDYvakWDPVhC6e9euPW32JunxK7I1zd9FUw34/MA4TCn1lYW5xt1eruIiBZwsgvQZwaFl0VLE2gHDdrepAP6Jg7R/30KbyuffIm3aQDIcvSh6DOs8AvByH2cKjmH9DYMfTJpRWrHvO+Rx3vvZG24+S7Klzalm8F3k5fx73Uaj9So8xuPESpuJzQ3zXDcIgpExTAqTSgv4c2jgS1/Q== rsa-key-20180604" >> /home/devops.interview/.ssh/authorized_keys
chown -R devops.interview:devops.interview /home/devops.interview/.ssh/
chmod 644 /home/devops.interview/.ssh/authorized_keys