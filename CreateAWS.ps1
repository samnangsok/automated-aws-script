#Creating new VPC with giving CIDR Address
$vpc = New-EC2Vpc -CidrBlock 10.255.0.0/16 
$vpcId = $vpc.VpcId
Write-Host vpcId = $vpcId

#Creating Internet Gateway with Add it to the VPC above
$InternetGateWay = New-EC2InternetGateway
$InternetGateWayId = $InternetGateWay.InternetGatewayId
Write-Host InternetGateWayId = $InternetGateWayId
Add-EC2InternetGateway -InternetGatewayId $InternetGateWayId -VpcId $vpcId

#Creating Routing Table with the VPC above
$routingTable = New-EC2RouteTable -VpcId $vpcId 
$routingTableId = $routingTable.RouteTableId
Write-Host RoutingTableId = $routingTableId
#Create Subnet1 & associate route table and VPC above
$subNet = New-EC2Subnet -VpcId $vpcId -CidrBlock 10.255.1.0/24
$subNetId = $subNet.SubnetId
Write-Host SubNetId = $subNetId
Register-EC2RouteTable -RouteTableId $routingTableId -SubnetId $subNetId

#Creating Security Group with RDP restriction and permit inbound HTTP/HTTPS traffic
$SecrurityGroupId = New-EC2SecurityGroup -GroupName security-group -Description "Security Group" -VpcId $vpcId
$ip1 = @{IpProtocol="tcp"; FromPort="3389"; ToPort="3389"; IpRanges='172.16.1.1/32'}
$ip2 = @{IpProtocol="tcp"; FromPort="80"; ToPort="80"; IpRanges='0.0.0.0/0'}
$ip3 = @{IpProtocol="tcp"; FromPort="433"; ToPort="433"; IpRanges='0.0.0.0/0'}
$ip4 = @{IpProtocol="-1"; FromPort="0"; ToPort="65535"; IpRanges='0.0.0.0/0'}
Grant-EC2SecurityGroupIngress -GroupId $SecrurityGroupId -IpPermissions @($ip1, $ip2, $ip3, $ip4)
Write-Host SecrurityGroupId = $SecrurityGroupId
#Creating EBS Volumn

$BootVolume = New-Object Amazon.EC2.Model.EbsBlockDevice
$BootVolume.VolumeSize = 20
$BootVolume.VolumeType = 'standard'

$DeviceMapping1 = New-Object Amazon.EC2.Model.BlockDeviceMapping
$DeviceMapping1.DeviceName = '/dev/sda1'
$DeviceMapping1.Ebs = $BootVolume

$StorageVolume = New-Object Amazon.EC2.Model.EbsBlockDevice
$StorageVolume.VolumeSize = 10
$StorageVolume.VolumeType = 'standard'

$DeviceMapping2 = New-Object Amazon.EC2.Model.BlockDeviceMapping
$DeviceMapping2.DeviceName = 'xvdf'
$DeviceMapping2.Ebs = $StorageVolume

#Creating EC2 instance
$Ec2Instance = New-EC2Instance -ImageId ami-307fa852 -MinCount 1 -MaxCount 1 -KeyName TruliooInterview -SecurityGroupId $SecrurityGroupId -Region ap-southeast-2 -InstanceType t1.micro -SubnetId $subNetId -DisableApiTermination $true -BlockDeviceMapping $DeviceMapping1, $DeviceMapping2

